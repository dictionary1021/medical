
package com.vis;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ReadFile
 */

public class ReadFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		//out.println("ok");
		//System.out.println("get the request");
		ServletContext context = request.getServletContext();
		String filePath = context.getInitParameter("file-upload");
		Vector<String> outFileLists= new Vector<String>(); 
		if(getFileList(filePath,outFileLists)){
			for(String str:outFileLists){
				//System.out.println(str);
				out.print(str);
				out.print(",");
			}
		}
	}
	private boolean  getFileList(String filePath,Vector<String> outFileLists){
		if(outFileLists == null)  
        {  
            outFileLists = new Vector<String>();  
        }  
        File file = new File(filePath);  
        if(file.exists())  
        {  
            File files[] = file.listFiles();  
            
             
                for(int i = 0; i < files.length; i++)  
                {  
                    if(files[i].isFile())  
                    {  
                        outFileLists.add(files[i].getName());  
                    }  
                }  
             
        }  
        else  
        {  
            return false;  
        }  
        return true;  
    
		
	}

}
