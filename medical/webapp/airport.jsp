<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
  <script type="text/javascript" src="./js/d3/d3.v3.js"></script>
    <!-- <link type="text/css" rel="stylesheet" href="./style.css"/>  -->
   
    <style type="text/css">

#states path {
  fill: #ccc;
  stroke: #fff;
}

path.arc {
  pointer-events: none;
  fill: none;
  stroke: #000;
  display: none;
}

path.cell {
  fill: none;
  pointer-events: all;
}

circle {
  fill: steelblue;
  fill-opacity: .8;
  stroke: #fff;
}

#cells.voronoi path.cell {
  stroke: brown;
}

#cells g:hover path.arc {
  display: inherit;
}

    </style>
  </head>
  <body>
  <div id="container" >
  </div>
    
    
  <script type="text/javascript" src="./js/d3/d3.v3.js"></script>
    <script type="text/javascript" src="./js/d3/d3.v2.js"></script>
    <script type="text/javascript" src="./js/d3/d3.geo.js"></script>
    <script type="text/javascript" src="./js/d3/d3.geom.js"></script>
    <script type="text/javascript" src="./js/d3/d3.layout.js"></script>
    <script type="text/javascript" src="./js/map.js"> </script>
  </body>
</html>
