<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css" />

<script type="text/javascript" src="./js/d3/d3.v3.js"></script>
    <!-- 
    <script type="text/javascript" src="./js/d3/d3.v2.js"></script> 
    --> 
    <script type="text/javascript" src="./js/d3/d3.geo.js"></script>
    <script type="text/javascript" src="./js/d3/d3.geom.js"></script> 
    <!-- 
    <script type="text/javascript" src="./js/d3/d3.layout.js"></script> 
    -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Medical Vis</title>
</head>
<body>

<div id="container">
<div id="sidebar"><!-- 
<input type="text" value="File"></input>
<input type="button" value="File"></input> -->
</div>
<div id="content" >
<div id="top" >
<p align="center" style="margin-top:0px;">Visualization Panel</p>
<ul> 
<li><a href="#"><span>Map</span></a></li> 
<li><a href="#"><span>Bar</span></a></li> 
<li><a href="#"><span>Line</span></a></li> 
<li><a href="#"><span>Heat</span></a></li> 
</ul> 
</div>
<div id="bottom" >
<div id ="map"> </div> 
<div id ="bar"> </div> 
<div id ="line" ><img alt="line" src="./data/line.png" style="width:900px;height:620px;"> </div> 
<div id ="heat" > 这里显示一张heatmap</div> 

</div>


<div id="toolbar">
<div id="toolbar-0">
<p align="center">Tool Bar</p>
<form>
<label for="year">Year:</label>
<select name="year" onchange="f(this)">
<option value="2010">2010</option>
<option value="2011">2011</option>
<option value="2012" selected="selected">2012</option>
<option value="2013">2013</option>
</select>
<hr>
<p>选择区域工具</p>
矩形：
<input type="radio"  name="select" value="rect" />
<br />
圈选：
<input type="radio" name="select" value="circle" />
<br />
点选：
<input type="radio" name="select" value="point" onclick="point()"/>
</form>
</div>

<div id="toolbar-1"><p align="center">Bar Toolbar</p> </div>
<div id="toolbar-2">Line Toolbar </div>
<div id="toolbar-3">this is toolbar 3</div>
</div>

<div id="info"><h2>Data Information: <span></span></h2></div>
</div>

</div>
<div id="my_custom_menu">
<input type="button" value="close" style="margin-right:0px;float:right;"onclick="document.getElementById('my_custom_menu').style.display='none'">
<br>
<br>
<hr/>
<input type="file" value="上传">
<input type="button" value="分析">
<br>
</div>
<div id="map_menu">
<input type="button" value="close" style="margin-right:0px;float:right;"onclick="document.getElementById('map_menu').style.display='none'">
<br>
<br>
<hr/>
<input type="button" value="对比" >
<br>
<hr/>
<input type="button" value="分析">
<br>
<hr/>
<input type="button" value="详细" onclick="detail()">
<br>
</div>

<div id="map_detail" >
<input type="button" value="close" style="margin-right:0px;float:right;"onclick="document.getElementById('map_detail').style.display='none'">
<br>
<br>
<hr/>
</div>
<script type="text/javascript" src="./js/map.js"> </script> 
<script type="text/javascript" src="./js/flare.js"> </script>  
 <script type="text/javascript" src="./js/bar.js"> </script>
 <!--   
  <script type="text/javascript" src="./js/line.js"> </script> --> 
  <script type="text/javascript" src="./js/donut.js"> </script>
  
  <script type="text/javascript" src="./js/drag.js"> </script>
<script type="text/javascript"> 
function $(o){
	return document.getElementById(o)
	} 
var ospan=$('top').getElementsByTagName('span'); 
var odiv=$('bottom').getElementsByTagName('div'); 
var divs=$('toolbar').getElementsByTagName('div'); 
for(var i=0;i<ospan.length;i++){ 
	ospan[i].alt=i; 
	ospan[i].onclick=function(){ 
	var nid=parseInt(this.alt);
	hidAll(); 
	this.style.backgroundColor='#fff'; 
	odiv[nid].style.display='block'; 
	divs[nid].style.display='block'; 
	} 
} 
function hidAll(){ 
for(var i=0;i<odiv.length;i++){ 
	odiv[i].style.display='none'; 
	divs[i].style.display='none'; 
	ospan[i].style.backgroundColor=''; 
	} 
} 
function f(obj)
{
	var dataset=[];
	for(var i=0;i<35;++i){
		var newNumber=Math.random()*100+1;
		dataset.push(newNumber);
	}
	createMap(dataset);
	
}
function createMap(obj)
{
	
}
function point(){
	
}
function detail(){
	document.getElementById('map_menu').style.display='none'
	document.getElementById('map_detail').style.display='block';
	 /* document.getElementById('map_detail').style.left = (document.body.offsetWidth - 540) / 2; 
	 document.getElementById('map_detail').style.top = (document.body.offsetHeight - 170) / 2 + document.body.scrollTop; */
	  d3.select('#map_detail')
    .style('position','absolute')
      .style('left', "200px")
      .style('top',  "100px")
      .style('zIndex','3')
      .style('display', 'block');  
}

</script>  
</body>
</html>