var id = 0xffff;

    function create() {
        var duration = 5000 + Math.random() * 25000;

        //var div = d3.select("body").insert("div", "div");
        var div1 = d3.select("#content").insert("div","div1");
        div1.append("span")
                .style("width", "74px")
                .style("text-align", "right")
                .text(++id);

        div1.append("span")
                .attr("class", "progress")
                .style("width", "240px")
                .style("left", "80px")
                .append("span")
                .attr("class", "value")
                .transition()
                .duration(duration)
                .ease("linear")
                .style("width", "100%");

        div1.transition()
                .style("height", "20px")
                .transition()
                .delay(duration)
                .style("height", "0px")
                .remove();

        setTimeout(create, Math.random() * 10000);
    }

    create();
    create();
    create();
    create();
    create();