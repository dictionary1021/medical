var w = 600,
    h = 480;
      

var projection = d3.geo.azimuthal()
					.mode("equidistant")
					.origin([100, 50])
					.scale(600)
					.translate([w/2, h/3]);


var path = d3.geo.path()
    .projection(projection);

var svg = d3.select("#map").insert("svg:svg", "h2")
    .attr("width", w)
    .attr("height", h);

var states = svg.append("svg:g")
    .attr("id", "states");

var circles = svg.append("svg:g")
    .attr("id", "circles");

var cells = svg.append("svg:g")
    .attr("id", "cells");

d3.select("input[type=checkbox]").on("change", function() {
  cells.classed("voronoi", this.checked);
});
var colors = d3.scale.quantize()
				.range(["rgb(237,248,233)","rgb(186,228,179)","rgb(116,196,118)","rgb(49,163,84)","rgb(0,109,44)"]);
var dataset=[];
var filled=false;
var filledColor=[];	
d3.json("./data/china.json", function(collection) {
	for(var i=0;i<collection.features.length;++i){
		var newNumber=Math.random()*100+1;
		dataset.push(newNumber);
	}
	colors.domain([d3.min(dataset),d3.max(dataset)]);
  states.selectAll("path")
      .data(collection.features)
      .enter()
      .append("svg:path")
      .attr("d", path)
      .attr("class","rect")
      .on("mouseover",over)
      .on("contextmenu",mapMenu)
      .on("click",function(d,i){
    	 // var currColor=d3.select(this).style("fill");
    	 
    	  if(!filled){
    	  d3.select(this)
    	  .style("fill","orange");
    	  filled=true;
    	  }
    	  else{
    		  d3.select(this)
        	  .style("fill",function(){return filledColor[d.id]});
    		  filled=false;
    	  }
    	 
    	  
    	  
      })
      .style("fill", function(d,i) {
    	 
	   		//Get data value
	   		var value = dataset[i];
	   		
	   		if (value) {
	   			//If value exists…
	   			filledColor[d.id]=colors(value);
	   	   	 //console.log(colors(value));
		   		return colors(value);
	   		} else {
	   			//If value is undefined…
		   		return "grey";
	   		}
	   	 
	   });
  
});

 
	  var linksByOrigin = {},
	      countByAirport = {},
	      locationByAirport = {},
	      positions = [];

	  d3.csv("./data/china-city-capital.csv", function(airports) {

	    // Only consider airports with at least one flight.
	    airports = airports.filter(function(airport) {
	      if (!countByAirport[airport.iata]) {
	        var location = [+airport.longitude, +airport.latitude];
	        locationByAirport[airport.iata] = location;
	        positions.push(projection(location));
	        return true;
	      }
	    });

	    // Compute the Voronoi diagram of airports' projected positions.
	     /*var polygons = d3.geom.voronoi(positions);

	    var g = cells.selectAll("g")
	        .data(airports)
	      .enter()
	      .append("svg:g");
	    
	    g.append("svg:path")
        .attr("class", "cell")
        .attr("d", function(d, i) { return "M" + polygons[i].join("L") + "Z"; })
        .on("mouseover", function(d, i) { console.out(d); });

	    g.selectAll("path.arc")
        .data(function(d) { return linksByOrigin[d.iata] || []; })
      .enter()
      .append("svg:path")
        .attr("class", "arc")
        .attr("d", function(d) { return path(arc(d)); }); 
*/
	    
	    circles.selectAll("circle")
	        .data(airports)
	        .enter()
	        .append("svg:circle")
	        .attr("cx", function(d, i) { return positions[i][0]; })
	        .attr("cy", function(d, i) { return positions[i][1]; })
	        .attr("r", 4)
	        .style("fill","red")
	        .on("mouseover",function(d,i){
	        	d3.select("h2 span").text(d.Airport+d.Range);
	        	})
	  });
	  function over(d){
		  //console.log(d.properties.name);
		  d3.select("h2 span").text(d.properties.name);
		  
	  }
	  function out(d){
		  //console.log(d3.select(this));
		  
	  }
	  function mapMenu(d,i){
		  d3.event.preventDefault();
 	 d3_target = d3.select(d3.event.target);
	 
	 var position = d3.mouse(this);
	 var contextMenuShowing = false;
     if (d3_target.classed("rect")) {
    	 
    	 //alert("ok");
         contextMenuShowing = true;
         d = d3_target.datum();
         console.log(d);
         
         console.log(position);
         d3.select('#map_menu')
         .style('position','absolute')
           .style('left',position[0] + "px")
           .style('top', position[1] + "px")
           .style('display', 'block');

                
     }
	
}