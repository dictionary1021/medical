/**
 * js拖动类 目前一个对象只能够使一个容器得到拖动
 * @author yulipu
 
 
 * 用法2 模拟对话框
 *    <div id="outer">
 *        <div id="title">这里是标题</div>
 *        <div>这里是内容</div>
 *    </div>
 * var drag = new YDrag();
 * drag.drag({
 *         targetDragObj : document.getElementById('outer'),  //必须项 要拖动的目标对象
 *        srcDragObj : document.getElementById('title'),  //非必须项 指定标题部分div
 *        dragX:false,  //不允许横向拖动
 *        callback : function(){
                       //alert(this.x);  // x坐标
                       //alert(this.y);  // y坐标
                }  //指定一个回调函数 这个函数可以得到当前容器坐标位置 
 * });  
 */
var drag = new YDrag();
  drag.drag({
          targetDragObj : document.getElementById('map_detail'),  //必须项 要拖动的目标对象
         dragX : true,  //不允许横向拖动
         dragY : true,
  });  
 function YDrag() {
    var diffX = 0;  //当前鼠标和容器left距离的差  当前位置-这个差值就是容器的left值
    var diffY = 0;
    var _self = this;
    var options = {};
    
    var yE = {
        /**
         * 添加事件
         * @param ele 要添加事件的元素
         * @param type 事件类型 如click
         * @param fun 回调函数
         */
        bind : function(ele, type, fun) {
            if(ele.addEventListener) {  //FF
                ele.addEventListener(type, fun, false);
            }else if(ele.attachEvent) {  //IE
                ele.attachEvent('on' + type, fun);
            } else {  //DOM
                ele['on'+type] = fun;
            }
        },
    
        /**
         * 删除事件
         */
        unbind : function(ele, type, fun) {
            if(ele.removeEventListener) {
                ele.removeEventListener(type, fun, false);
            } else if(ele.detachEvent) {
                ele.detachEvent('on' + type, fun);
            } else {
                ele['on'+type] = null;
            }
        }
    };
    
    this._mousedown = function(e) {
        e = e || window.event;
        options.targetDragObj.style.position = 'absolute';
        
        diffX = e.clientX - options.targetDragObj.offsetLeft;  //初始化差值
        diffY = e.clientY - options.targetDragObj.offsetTop;
        yE.bind(document, 'mousemove', _self._mousemove);  //往document上添加事件 当鼠标在document范围内移动式都触发移动事件
        yE.bind(document, 'mouseup', _self._mouseup);
    };
    
    this._mousemove = function(e) {
        e = e || window.event;
        if(options.dragable) {
            options.dragX && (options.targetDragObj.style.left = e.clientX - diffX + 'px');
            options.dragY && (options.targetDragObj.style.top =  e.clientY - diffY + 'px');
            if(options.callback){
                //返回当前容器坐标
                var obj = {'x' : e.clientX - diffX, 'y' : e.clientY - diffY};
                options.callback.call(obj);
            }
        }
    };
    
    this._mouseup = function(e) {
        yE.unbind(document, 'mousemove', _self._mousemove);
        yE.unbind(document, 'mouseup', _self._mouseup);
    };
    
    this.drag = function(opt) {
        options = {
            dragable : true,
            targetDragObj : opt.targetDragObj,
            srcDragObj : opt.srcDragObj,
            dragX : opt.dragX != false,  //横向拖动
            dragY : opt.dragY != false,  //纵向拖动
            callback : opt.callback
        };
        if(options.srcDragObj) {options.srcDragObj.style.cursor='move';} else {options.targetDragObj.style.cursor='move'}
        undefined == options.srcDragObj ? 
            yE.bind(options.targetDragObj, 'mousedown', this._mousedown) :
            yE.bind(options.srcDragObj, 'mousedown', this._mousedown);
    };
 }
